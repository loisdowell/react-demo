import React, { Component } from "react";
import { connect } from "react-redux";

// Bootstrap-librray
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

// Css file
import "./App.css";

// Actions
import { fetchPostList } from "./actions/Post-action";
import { fetchAddPost } from "./actions/addPost";

import Routes from "./Routes";

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { postsData } = this.props;
    // console.log("props data",data)
    return (
      <React.Fragment>
        <Routes />

        <ToastContainer
          position="top-right"
          autoClose={3000}
          hideProgressBar
          newestOnTop
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    postsLoading: state.posts.isLoading,
    postsData: state.posts?.data || [],
    postsError: state.posts?.error || {},

    addPostLoading: state.addPost.isLoading,
    addPostData: state.addPost?.data || [],
    addPostError: state.addPost?.error || {},
  };
};

const mapDispatchToProps = {
  fetchPostList,
  fetchAddPost,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
