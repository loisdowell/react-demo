import {
    FETCH_ADD_POST_REQUEST,
    FETCH_ADD_POST_SUCCESS,
    FETCH_ADD_POST_FAILURE,
  } from '../actions/types';
  
  const initialState = {
    data: null,
    error: null,
    isLoading: false,
  };
  
  const AddPostReducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_ADD_POST_REQUEST:
        return {
          ...state,
          isLoading: true,
        };
      case FETCH_ADD_POST_SUCCESS:
        return {
          ...state,
          isLoading: false,
          data: action.data.data,
          error: null,
        };
      case FETCH_ADD_POST_FAILURE:
        return {
          ...state,
          isLoading: false,
          data: null,
          error: action.error.response,
        };
      default:
        return state;
    }
  };
  
  export default AddPostReducer;
  