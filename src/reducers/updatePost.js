import {
    FETCH_UPDATE_POST_REQUEST,
    FETCH_UPDATE_POST_SUCCESS,
    FETCH_UPDATE_POST_FAILURE,
  } from '../actions/types';
  
  const initialState = {
    data: null,
    error: null,
    isLoading: false,
  };
  
  const UpdatePostReducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_UPDATE_POST_REQUEST:
        return {
          ...state,
          isLoading: true,
        };
      case FETCH_UPDATE_POST_SUCCESS:
        return {
          ...state,
          isLoading: false,
          data: action.data.data,
          error: null,
        };
      case FETCH_UPDATE_POST_FAILURE:
        return {
          ...state,
          isLoading: false,
          data: null,
          error: action.error.response,
        };
      default:
        return state;
    }
  };
  
  export default UpdatePostReducer;
  