import { combineReducers } from "redux";
import AddPostReducer from "./addPost";
import PostDetailReducer from "./details";
import LoginReducer from "./login";
import Postreducer from "./Postreducer";
import UpdatePostReducer from "./updatePost";

const rootReducer = combineReducers({
  posts: Postreducer,
  addPost: AddPostReducer,
  postDetail: PostDetailReducer,
  updatePost: UpdatePostReducer,

  login: LoginReducer,
});

export default rootReducer;
