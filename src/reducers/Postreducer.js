import {
    FETCH_POST_LIST_REQUEST,
    FETCH_POST_LIST_SUCCESS,
    FETCH_POST_LIST_FAILURE,
  } from '../actions/types';
  
  const initialState = {
    data: null,
    error: null,
    isLoading: false,
  };
  
  const Postreducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_POST_LIST_REQUEST:
        return {
          ...state,
          isLoading: true,
        };
      case FETCH_POST_LIST_SUCCESS:
        return {
          ...state,
          isLoading: false,
          data: action.data.data,
          error: null,
        };
      case FETCH_POST_LIST_FAILURE:
        return {
          ...state,
          isLoading: false,
          data: null,
          error: action.error.response,
        };
      default:
        return state;
    }
  };
  
  export default Postreducer;
  