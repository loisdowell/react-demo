import {
    FETCH_POST_DETAILS_REQUEST,
    FETCH_POST_DETAILS_SUCCESS,
    FETCH_POST_DETAILS_FAILURE,
  } from '../actions/types';
  
  const initialState = {
    data: null,
    error: null,
    isLoading: false,
  };
  
  const PostDetailReducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_POST_DETAILS_REQUEST:
        return {
          ...state,
          isLoading: true,
        };
      case FETCH_POST_DETAILS_SUCCESS:
        return {
          ...state,
          isLoading: false,
          data: action.data.data,
          error: null,
        };
      case FETCH_POST_DETAILS_FAILURE:
        return {
          ...state,
          isLoading: false,
          data: null,
          error: action.error.response,
        };
      default:
        return state;
    }
  };
  
  export default PostDetailReducer;
  