import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import PostList from "./components/Post/list";
import addEditPost from "./components/Post/addEditPost";
import Authenticate from "./components/Authenticate";
import Login from "./components/Login";

// const TheLayout = React.lazy(() => import("./containers/TheLayout"));

// const AuthRoute = ({ component: Component, ...rest }) => {
//   const authToken = localStorage.getItem("authToken");

//   return (
//     <Route
//       {...rest}
//       render={(props) =>
//         authToken ? <TheLayout {...props} /> : <Redirect to="/login" />
//       }
//     />
//   );
// };

class Routes extends Component {
  render() {
    return (
      <Router>
        <React.Suspense>
          <Switch>
            <Route exact name="login" path="/login" component={Login} />
            <Route
              exact
              name="post-list"
              path="/post-list"
              component={Authenticate(PostList)}
            />

            <Route
              exact
              name="post-list"
              path="/edit-post/:id"
              component={Authenticate(addEditPost)}
            />

            <Route
              exact
              name="post-list"
              path="/add-post"
              component={Authenticate(addEditPost)}
            />
            <Route path="/" render={() => <Redirect to="/login" />} />
          </Switch>
        </React.Suspense>
      </Router>
    );
  }
}

export default Routes;
