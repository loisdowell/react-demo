import React, { Component } from "react";
import { connect } from "react-redux";

// Actions
import { fetchPostList } from "../../actions/Post-action";
import { fetchAddPost } from "../../actions/addPost";
import { fetchPostDetail } from "../../actions/details";
import { fetchUpdatePost } from "../../actions/updatePost";
import { CButton } from "@coreui/react";

class AddEditPost extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.fetchDetails();
    this.getPostId();
  }

  getPostId = () => {
    return this.props?.match?.params?.id || "";
  };

  fetchDetails = () => {
    this.props.fetchPostDetail(this.getPostId());
  };

  updateValue = () => {
    const apiVal = {
      id: this.getPostId(),
      title: "title",
      body: "skldfj",
    };

    if (this.getPostId()) {
      this.props.fetchUpdatePost(apiVal).then(() => {
        const { updatePostData, history } = this.props;
        if (updatePostData) {
          history.push("/post-list");
        }
      });
    } else {
      this.props.fetchAddPost(apiVal).then(() => {
        const { updatePostData, history } = this.props;
        if (updatePostData) {
          history.push("/post-list");
        }
      });
    }
  };

  render() {
    const { postDetailData } = this.props;
    console.log("TCL ~ AddEditPost ~ render ~ postDetailData", postDetailData);
    return (
      <React.Fragment>
        <div className="App">
          <CButton onClick={this.updateValue}>Update</CButton>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    postDetailLoading: state.postDetail.isLoading,
    postDetailData: state.postDetail?.data || [],
    postDetailError: state.postDetail?.error || {},

    updatePostLoading: state.updatePost.isLoading,
    updatePostData: state.updatePost?.data || [],
    updatePostError: state.updatePost?.error || {},
  };
};

const mapDispatchToProps = {
  fetchPostDetail,
  fetchUpdatePost,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddEditPost);
