import React, { Component } from "react";
import { connect } from "react-redux";

// Actions
import { fetchPostList } from "../../actions/Post-action";
import { fetchAddPost } from "../../actions/addPost";
import { CButton } from "@coreui/react";

class PostList extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.getList();
  }

  getList = () => {
    this.props.fetchPostList().then(() => {
      const { postsData, postsError } = this.props;
    });
  };

  addData = () => {
    const values = { title: "new_title", body: "new_body", userId: "userid" };
    this.props.fetchAddPost(values).then(() => {
      const { addPostData } = this.props;

      if (addPostData) {
        this.getList();
      }
    });
  };

  gotoEditPage = (id) => {
    const { history } = this.props;
    history.push(`/edit-post/${id}`);
  };

  render() {
    const { postsData } = this.props;
    return (
      <React.Fragment>
        <div className="App">
          <CButton onClick={this.addData}>Add new</CButton>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>TITLE</th>
                <th>BODY</th>
              </tr>
            </thead>
            <tbody>
              {postsData.map((d) => {
                return (
                  <tr key={d.id}>
                    <td>{d.id}</td>
                    <td>{d.title}</td>
                    <td>{d.body}</td>
                    <td>
                      <button
                        className="btn btn-primary"
                        onClick={() => {
                          this.gotoEditPage(d.id);
                        }}
                      >
                        Edit
                      </button>
                    </td>
                    <td>
                      <button className="btn btn-danger">Delete</button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    postsLoading: state.posts.isLoading,
    postsData: state.posts?.data || [],
    postsError: state.posts?.error || {},

    addPostLoading: state.addPost.isLoading,
    addPostData: state.addPost?.data || [],
    addPostError: state.addPost?.error || {},
  };
};

const mapDispatchToProps = {
  fetchPostList,
  fetchAddPost,
};

export default connect(mapStateToProps, mapDispatchToProps)(PostList);
