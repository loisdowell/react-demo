import React, { Component } from "react";
import { connect } from "react-redux";
import LaddaButton, { XL, SLIDE_UP } from "@zumper/react-ladda";

// Actions
import { fetchPostList } from "../../actions/Post-action";
import { fetchAddPost } from "../../actions/addPost";
import { fetchLogin } from "../../actions/login";
import { CButton } from "@coreui/react";

class Login extends Component {
  state = {
    email: "",
    password: "",
  };
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onSubmitButton = () => {
    this.props.fetchLogin(this.state);
  };

  render() {
    const { loginLoading } = this.props;
    return (
      <form>
        <div class="mb-3">
          <label for="exampleInputEmail1">Email address</label>
          <input
            type="email"
            class="form-control"
            id="exampleInputEmail1"
            name="email"
            onChange={this.handleChange}
          />
          <small id="emailHelp" class="form-text text-muted">
            We'll never share your email with anyone else.
          </small>
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1">Password</label>
          <input
            type="password"
            class="form-control"
            id="exampleInputPassword1"
            name="password"
            onChange={this.handleChange}
          />
        </div>
        <LaddaButton
          loading={loginLoading}
          onClick={this.onSubmitButton}
          color="mint"
          size={XL}
          style={SLIDE_UP}
          spinnerSize={30}
          spinnerColor="#ddd"
          spinnerLines={12}
        >
          Click Here!
        </LaddaButton>
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loginLoading: state.login.isLoading,
    loginData: state.login?.data || [],
    loginError: state.login?.error || {},
  };
};

const mapDispatchToProps = {
  fetchPostList,
  fetchAddPost,
  fetchLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
