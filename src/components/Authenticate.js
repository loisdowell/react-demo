import React from "react";
import { Redirect } from "react-router-dom";

const Authenticate = (Component) => {
  return class AuthenticatedComponent extends React.Component {
    /**
     * Check if the user is authenticated, this.props.isAuthenticated
     * has to be set from your application logic (or use react-redux to retrieve it from global state).
     */
    isAuthenticated = () => {
      return localStorage.getItem("authenticationToken") == "";
    };

    /**
     * Render
     */
    render() {
      console.log("TCL ~ AuthenticatedComponent ~ render ~ render");
      const loginErrorMessage = (
        <div>
          Please <a href="/login">login</a> in order to view this part of the
          application.
        </div>
      );

      console.log(
        "TCL ~ AuthenticatedComponent ~ render ~ this.isAuthenticated",
        this.isAuthenticated()
      );

      return (
        <div>
          {this.isAuthenticated() === true ? (
            <Component {...this.props} />
          ) : (
            loginErrorMessage
          )}
        </div>
      );
    }
  };
};

export default Authenticate;
