import {
  FETCH_UPDATE_POST_REQUEST,
  FETCH_UPDATE_POST_SUCCESS,
  FETCH_UPDATE_POST_FAILURE,
} from "./types";
import axios from "axios";
import { apiToastError, apiToastSuccess } from "../util/common";

export const fetchUpdatePostRequest = () => ({
  type: FETCH_UPDATE_POST_REQUEST,
});

export const fetchUpdatePostSuccess = (data) => ({
  type: FETCH_UPDATE_POST_SUCCESS,
  data,
});

export const fetchUpdatePostFailure = (error) => ({
  type: FETCH_UPDATE_POST_FAILURE,
  error,
});

export const fetchUpdatePost = (formData) => (dispatch) => {
  dispatch(fetchUpdatePostRequest());

  const config = {
    // headers: { "Content-Type": "application/x-www-form-urlencoded" },
    // responseType: "blob",
  };

  return axios
    .put(`https://jsonplaceholder.typicode.com/posts/${formData.id}`)
    .then((data) => {
      console.log("TCL ~ .then ~ data", data.data);
      if (data?.data) {
        apiToastSuccess("Successfully Updated");
        dispatch(fetchUpdatePostSuccess(data));
      } else {
        apiToastError("Something went wrong");
        dispatch(fetchUpdatePostFailure(data));
      }
    })
    .catch((error) => {
      apiToastError("Something went wrong");
      dispatch(fetchUpdatePostFailure(error));
    });
};
