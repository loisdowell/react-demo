import {
  FETCH_ADD_POST_REQUEST,
  FETCH_ADD_POST_SUCCESS,
  FETCH_ADD_POST_FAILURE,
} from "./types";
import axios from "axios";
import { apiToastError, apiToastSuccess } from "../util/common";

export const fetchAddPostRequest = () => ({
  type: FETCH_ADD_POST_REQUEST,
});

export const fetchAddPostSuccess = (data) => ({
  type: FETCH_ADD_POST_SUCCESS,
  data,
});

export const fetchAddPostFailure = (error) => ({
  type: FETCH_ADD_POST_FAILURE,
  error,
});

export const fetchAddPost = (formData) => (dispatch) => {
  const apiVal = {
    ...formData,
  };

  dispatch(fetchAddPostRequest());

  const config = {
    // headers: { "Content-Type": "application/x-www-form-urlencoded" },
    // responseType: "blob",
  };

  return axios
    .post(`https://jsonplaceholder.typicode.com/posts`, apiVal)
    .then((data) => {
      console.log("TCL ~ .then ~ data", data.data);
      if (data?.data) {
        apiToastSuccess("Successfully Added");
        dispatch(fetchAddPostSuccess(data));
      } else {
        apiToastError("Something went wrong");
        dispatch(fetchAddPostFailure(data));
      }
    })
    .catch((error) => {
      apiToastError("Something went wrong");
      dispatch(fetchAddPostFailure(error));
    });
};
