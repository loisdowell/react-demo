import axios from "axios";
import {
  FETCH_LOGIN_REQUEST,
  FETCH_LOGIN_SUCCESS,
  FETCH_LOGIN_FAILURE,
} from "../types/login";
import {
  apiToastError,
  apiToastSuccess,
  getRandomString,
} from "../util/common";

export const fetchLoginRequest = () => ({
  type: FETCH_LOGIN_REQUEST,
});

export const fetchLoginSuccess = (data) => ({
  type: FETCH_LOGIN_SUCCESS,
  data,
});

export const fetchLoginFailure = (error) => ({
  type: FETCH_LOGIN_FAILURE,
  error,
});

export const fetchLogin = (formData) => (dispatch) => {
  const apiVal = {
    ...formData,
  };

  dispatch(fetchLoginRequest());

  const config = {
    // headers: { "Content-Type": "application/x-www-form-urlencoded" },
    // responseType: "blob",
  };

  return axios
    .post(`https://jsonplaceholder.typicode.com/posts`, apiVal, config)
    .then((data) => {
      if (data?.data) {
        const token = getRandomString();

        localStorage.setItem("authenticationToken", token);

        apiToastSuccess("Successfully Login");
        dispatch(fetchLoginSuccess(data));
      } else {
        apiToastError("Something went wrong");
        dispatch(fetchLoginFailure(data));
      }
    })
    .catch((error) => {
      apiToastError("Something went wrong");
      dispatch(fetchLoginFailure(error));
    });
};
