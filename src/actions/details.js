import {
  FETCH_POST_DETAILS_REQUEST,
  FETCH_POST_DETAILS_SUCCESS,
  FETCH_POST_DETAILS_FAILURE,
} from "./types";
import axios from "axios";
import { apiToastError, apiToastSuccess } from "../util/common";

export const fetchPostDetailRequest = () => ({
  type: FETCH_POST_DETAILS_REQUEST,
});

export const fetchPostDetailSuccess = (data) => ({
  type: FETCH_POST_DETAILS_SUCCESS,
  data,
});

export const fetchPostDetailFailure = (error) => ({
  type: FETCH_POST_DETAILS_FAILURE,
  error,
});

export const fetchPostDetail = (id) => (dispatch) => {
  dispatch(fetchPostDetailRequest());

  const config = {
    // headers: { "Content-Type": "application/x-www-form-urlencoded" },
    // responseType: "blob",
  };

  return axios
    .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then((data) => {
      console.log("TCL ~ .then ~ data", data.data);
      if (data?.data) {
        dispatch(fetchPostDetailSuccess(data));
      } else {
        dispatch(fetchPostDetailFailure(data));
      }
    })
    .catch((error) => {
      dispatch(fetchPostDetailFailure(error));
    });
};
