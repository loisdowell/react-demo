import {
  FETCH_POST_LIST_REQUEST,
  FETCH_POST_LIST_SUCCESS,
  FETCH_POST_LIST_FAILURE,
} from "./types";
import axios from "axios";

export const fetchPostListRequest = () => ({
  type: FETCH_POST_LIST_REQUEST,
});

export const fetchPostListSuccess = (data) => ({
  type: FETCH_POST_LIST_SUCCESS,
  data,
});

export const fetchPostListFailure = (error) => ({
  type: FETCH_POST_LIST_FAILURE,
  error,
});

export const fetchPostList = (formData) => (dispatch) => {
  const passVal = {
    ...formData,
  };

  dispatch(fetchPostListRequest());

  const config = {
    // headers: { "Content-Type": "application/x-www-form-urlencoded" },
    // responseType: "blob",
  };

  return axios
    .get(`https://jsonplaceholder.typicode.com/posts`)
    .then((data) => {
      dispatch(fetchPostListSuccess(data));
      // console.log("data from axios",data)
    })
    .catch((error) => {
      dispatch(fetchPostListFailure(error));
    });
};
